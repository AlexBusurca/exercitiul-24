package Ro.Orange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<String> alpha = Arrays.asList(
                new String("a"),
                new String("b"),
                new String("c"),
                new String("d")
        );

        List UpperAlpha = alpha.stream().map(s->s.toUpperCase())
                          .collect(Collectors.toCollection(ArrayList::new));

        System.out.println(UpperAlpha);

        List<Integer> numbers = Arrays.asList(
                new Integer(5),
                new Integer(10),
                new Integer(15),
                new Integer(20),
                new Integer(21978)
        );

        List DoubleNumbers = numbers.stream().map(n->n*2)
                            .collect(Collectors.toCollection(ArrayList::new));

        System.out.println(DoubleNumbers);

    }
}
